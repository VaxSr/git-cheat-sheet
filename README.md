# Controllo di Versione

## Importanza del Controllo di Versione

### Sviluppo Collaborativo di Progetti

Il controllo di versione consente a più sviluppatori di collaborare contemporaneamente su un progetto senza sovrascrivere le modifiche altrui, integrando le modifiche in modo ordinato e controllato.

### Storico delle Modifiche

Il controllo di versione mantiene uno storico dettagliato delle modifiche al codice sorgente, permettendo di tracciare chi ha fatto cosa e quando, facilitando il debug e la comprensione dell'evoluzione del progetto.

---

> [!NOTE] Non solo Git.
> Il controllo di versione non si limita a Git. Esistono diversi altri sistemi che offrono funzionalità di gestione delle versioni:
>
> - **SVN (Subversion)**
> - **Mercurial**
> - **Concurrent Versions System (CVS)**
> - **BitKeeper**
> - **Bazaar**
> - **Amazon CodeCommit**
> - **Azure DevOps Server**

# Definizioni e Comandi Git

## Definizioni Fondamentali

- **Repository**: Un archivio di codice sorgente che può essere locale o remoto. Un repository contiene tutte le versioni storiche del codice, permettendo la gestione e il tracciamento delle modifiche.
- **Push**: Il processo di inviare le modifiche locali al repository remoto. Questo è comunemente usato per aggiornare il codice condiviso con altre persone o con un server centrale.

- **Fetch/Pull**: Il processo di ricevere modifiche dal repository remoto. `Fetch` recupera le modifiche senza applicarle, mentre `Pull` combina il `fetch` con un `merge`, integrando le modifiche nel branch corrente.

## `.gitignore`

si mettono le regole (file) che volete che git non veda

## Comandi Git Essenziali

### `git add`

- **Scopo**: Aggiunge i file allo stage (area di preparazione per il commit).

```shell
git add nomeFile.txt
```

- **Uso parziale**: L'opzione `-p` (o `--patch`) permette di aggiungere solo parti specifiche di un file, facilitando un commit più granulare e preciso.

```shell
git add -p nomeFile.txt
```

### `git commit`

- **Scopo**: Crea un nuovo commit basato sui file nello stage. Il commit include un messaggio che descrive le modifiche effettuate.
- **Uso**: `git commit -m "messaggio"` per un commit semplice con messaggio.

```shell
git commit -m "Messaggino fantastico per il mio team"
```

### `git commit --amend`

modifica l'ultimo messaggio di commit

### `git status`

- **Scopo**: Visualizza lo stato dei file rispetto allo stage e al repository, mostrando file modificati, aggiunti o eliminati.
- **Uso**: `git status` per un resoconto dello stato corrente.

```shell
git status
```

### `git rm`

- **Scopo**: Rimuove file dal repository.
- **Opzione**: Con `--cached`, il file viene rimosso dal repository, ma resta sul disco locale.

```shell
git rm file.txt
git rm --cached file.txt
```

### `git mv`

- **Scopo**: Rinomina o sposta file nel repository.
- **Uso**: `git mv vecchio_nome nuovo_nome` per rinominare o spostare un file.

```shell
git mv vecchio_nome.txt nuovo_nome.txt
```

### `git log`

- **Scopo**: Visualizza la cronologia dei commit con dettagli come commit ID, autore, data e messaggio del commit.
- **Uso**: `git log` per vedere i log.

```shell
git log
```

### `git show`

- **Scopo**: Mostra i dettagli di un singolo commit, inclusi i cambiamenti apportati.
- **Uso**: `git show commit_id` per visualizzare cosa è cambiato in un commit specifico.

```shell
git show 1234abcd(codice commit)
```

### `git diff`

- **Scopo**: Mostra le differenze tra due commit, o tra il codice attuale e l'ultimo commit.
- **Opzione**: `--stat` per una statistica dei cambiamenti (ad esempio, numero di linee aggiunte e rimosse).

### `git checkout`

- **Scopo**: Navigare tra le versioni del codice o creare nuovi branch.
- **Uso**: `git checkout nome_branch` per cambiare branch, `git checkout commit_id` per tornare a una versione specifica. `-b` per creare e spostarsi su un nuovo branch.

```shell
git checkout main
git checkout -b piccolaFeature
```

### `git merge`

- **Scopo**: Unisce le modifiche da un branch in un altro.
- **Uso**: `git merge branch_da_unire` per unire le modifiche di un altro branch nel branch corrente. (STARE NEL MAIN E POI FARE MERGE DA UN ALTRO BRANCH)

```shell
git checkout main
git merge feature_branch
```

## `Repository Remoti`

Un repository remoto è una copia di un repository Git (o altro sistema di controllo di versione) che è ospitata su un server remoto, piuttosto che sul computer locale di uno sviluppatore.
Questo server remoto può essere un servizio cloud come

- **GitHub**
- **GitLab**
- **Bitbucket**
  servono per ospitare repository Git.

## `SSH Key ed25519`

- La chiave pubblica va poi inserita nel profilo nella sezione `ssh-keys` così ci facilità l'autenticazione.

```shell
ssh-keygen -t ed25519
```

### `git clone`

- **Scopo**: Crea una copia locale di un repository remoto.

```shell
git clone https://github.com/utente/progetto.git
```

### `git push`

- **Scopo**: Invia le modifiche dal repository locale al repository remoto

```shell
git push origin piccolaFeature
```

### `git fetch`

- **Scopo**: Recupera le modifiche dal repository remoto senza modificarne la copia locale.
- **Uso**: Questo comando recupera le modifiche dal repository remoto `origin`. Le modifiche vengono scaricate ma non integrate nel branch locale corrente.

```bash
git fetch origin
```

![](https://miro.medium.com/v2/resize:fit:1400/1*gZX2Cs-To3k1h63hHhPPcw.png)

## Comandi Avanzati

### `git rebase`

- **Scopo**: Riallinea un branch con il branch master, riscrivendo la cronologia dei commit.
- **Uso**: `git rebase master` per allineare il branch corrente al master. Attenzione a dove viene lanciato.

```bash
git rebase master
```

### `git reset`

- **Scopo**: Annulla uno o più commit, ripristinando lo stato precedente.
- **Uso**: `git reset --hard commit_id` per eliminare definitivamente i commit successivi a `commit_id`. Utilizzato con cautela su repository remoti.

### `git revert`

- **Scopo**: Crea un nuovo commit che annulla le modifiche di un commit precedente.
- **Uso**: `git revert commit_id` per creare un commit che annulla le modifiche di `commit_id`.

```bash
git revert 1234abcd(codice commit)
```

### `git cherry-pick`

- **Scopo**: Copia un commit specifico da un branch a un altro.
- **Uso**: `git cherry-pick commit_id` per applicare un commit selezionato al branch corrente.

```bash
git cherry-pick 1234abcd(codice commit)
```

### `git blame`

- **Scopo**: Mostra chi ha modificato quale riga di un file e in quale commit.
- **Opzioni**: `-M` e `-C` per identificare le righe che sono state spostate o copiate.

```bash
git blame file.txt
```

### `git tag`

- **Scopo**: Crea un tag per un commit specifico, utile per marcare versioni di rilascio o altri punti importanti nella storia del progetto.
- **Uso**: `git tag nome_tag commit_id` per aggiungere un tag.

```bash
git tag v1.0 1234abcd
```

## Operazioni Speciali

- **Forzare il push**: `git push -f origin branch` per forzare il push su un branch remoto, sovrascrivendo le modifiche.

```bash
git push -f origin feature_branch
```

- **Modificare un commit**: `git commit --amend` per modificare l'ultimo commit, aggiornandone il messaggio o aggiungendo/rimuovendo file.

```bash
git commit --amend -m "Nuovo messaggio"
```

## Precommit Hooks

- **Scopo**: I precommit hooks sono script che vengono eseguiti prima di un commit per verificare la qualità del codice.
- **Uso**: Utilizzati per eseguire controlli e rimuovere codice non necessario automaticamente.

## Continuous Integration (CI)

- **Definizione**: Processo che automatizza la build, il test e il deploy del codice ad ogni push.
- **Stages**: Fasi sequenziali di CI che possono includere build, test e deploy.
- **Esempio YML**:

```yml
stages:
  - build
  - test
  - deploy
```
